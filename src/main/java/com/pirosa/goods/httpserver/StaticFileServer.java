package com.pirosa.goods.httpserver;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.util.resource.Resource;

public class StaticFileServer implements AutoCloseable {

    private final Server server;

    public StaticFileServer() {
        this.server = new Server();
        ServerConnector connector = new ServerConnector(server);
        connector.setPort(8090);
        server.setConnectors(new Connector[] { connector });
        ContextHandler context0 = new ContextHandler();
        context0.setContextPath("/");
        ResourceHandler rh0 = new ResourceHandler();
        rh0.setBaseResource(Resource.newClassPathResource("/public"));
        context0.setHandler(rh0);
        ContextHandlerCollection contexts = new ContextHandlerCollection();
        contexts.setHandlers(new Handler[] { context0 });
        server.setHandler(contexts);
        server.setStopAtShutdown(true);
    }

    public void start() throws Exception {
        server.start();
    }

    @Override
    public void close() throws Exception {
        server.stop();
    }
}