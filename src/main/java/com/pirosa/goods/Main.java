package com.pirosa.goods;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.pirosa.goods.httpserver.StaticFileServer;
import com.pirosa.goods.websocketserver.ChatServer;

public class Main {

    public static void main(String[] args) {
        try (StaticFileServer staticFileServer = new StaticFileServer();
            ChatServer chatServer = new ChatServer()) {
            staticFileServer.start();
            chatServer.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Please press a key to stop the servers.");
            reader.readLine();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
