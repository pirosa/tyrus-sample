package com.pirosa.goods.websocketserver;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.websocket.Session;

public class ClientList {

    private Map<String, Session> sessions = new ConcurrentHashMap<>();

    private AtomicInteger number = new AtomicInteger(1);

    public void add(Session session) {
        UserUtils.setUserName(session, number.getAndIncrement());
        sessions.put(session.getId(), session);
    }

    public List<Session> getOthers(final Session session) {
        return sessions.entrySet().stream().filter(p -> !p.getKey().equals(session.getId())).map(Map.Entry::getValue)
            .collect(Collectors.toList());
    }

    public void remove(Session session) {
        sessions.remove(session.getId());
    }

}
