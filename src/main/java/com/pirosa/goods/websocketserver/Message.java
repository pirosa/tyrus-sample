package com.pirosa.goods.websocketserver;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Message {
    private final String sender;

    private final String text;

    @JsonCreator
    public Message(@JsonProperty("sender") String sender, @JsonProperty("message") String text) {
        this.sender = sender;
        this.text = Jsoup.clean(text, Whitelist.basic());
    }

    public String getSender() {
        return sender;
    }

    public String getText() {
        return text;
    }
}
