package com.pirosa.goods.websocketserver;

import org.glassfish.tyrus.server.Server;

public class ChatServer implements java.lang.AutoCloseable {

    private final Server server;

    public ChatServer() {
        server = new org.glassfish.tyrus.server.Server("localhost", 8025,
                "/chat", ChatEndpoint.class);
    }

    public void start() throws Exception {
        server.start();
    }

    public void stop() throws Exception {
        server.stop();
    }

    @Override
    public void close() throws Exception {
        server.stop();
    }
}
