package com.pirosa.goods.websocketserver;

import javax.websocket.Session;

public class UserUtils {
    private static final String USER_NAME_PREFIX = "user-";
    private final static String PEER_NAME = "name";

    public static String generateUserName(int index) {
        return USER_NAME_PREFIX + index;
    }

    public static String getUserName(Session session) {
        return (String) session.getUserProperties().get(PEER_NAME);
    }

    public static void setUserName(Session session, int index) {
        session.getUserProperties().put(UserUtils.PEER_NAME, generateUserName(index));
    }

    public static void setUserName(Session session, String username) {
        session.getUserProperties().put(UserUtils.PEER_NAME, username);
    }
}
