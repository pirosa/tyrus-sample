package com.pirosa.goods.websocketserver;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value = "/server")
public class ChatEndpoint {

    public static final String MESSAGE_FOR_SELF = "-YOU-";

    private Logger logger = Logger.getLogger(this.getClass().getName());

    private static ClientList clientsList = new ClientList();

    private ChatProtocol chatProtocol = new ChatProtocol();

    @OnOpen
    public void onOpen(Session session) {
        logger.info("Connected ... " + session.getId());
        clientsList.add(session);
        List<Session> others = clientsList.getOthers(session);
        chatProtocol.informOthersJoin(session, others);
        chatProtocol.welcomeNew(session, others);
    }

    @OnMessage
    public void onMessage(String message, Session session) throws IOException {
        chatProtocol.sendToOthers(clientsList.getOthers(session),
            new Message(UserUtils.getUserName(session), message));
        chatProtocol.sendMessage(session, new Message(MESSAGE_FOR_SELF, message));
    }

    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
        logger.info(String.format("Session %s closed because of %s", session.getId(), closeReason));
        chatProtocol.informOthersLeave(session, clientsList.getOthers(session));
        clientsList.remove(session);
    }
}