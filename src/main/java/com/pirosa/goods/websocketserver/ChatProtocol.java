package com.pirosa.goods.websocketserver;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.websocket.Session;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ChatProtocol {

    public static final String SERVER_MESSAGE = "-SERVER-";

    private ObjectMapper objectMapper = new ObjectMapper();

    public void welcomeNew(Session session, List<Session> others) {
        final String welcomeMessage;
        if (others.isEmpty()) {
            welcomeMessage = "Hello " + UserUtils.getUserName(session) + "! You are currently alone.";
        } else {
            String userList = others.stream().map(UserUtils::getUserName).collect(Collectors.joining("<LI>"));
            welcomeMessage = "Hello " + UserUtils.getUserName(session) + "! <BR><UL> <LH>Available users:</LH> <LI>"
                + userList
                + "</UL>";
        }
        sendMessage(session, new Message(SERVER_MESSAGE, welcomeMessage));
    }

    public void informOthersJoin(Session newSession, List<Session> others) {
        final String joinMessage = UserUtils.getUserName(newSession) + " joined to chat!";
        sendToOthers(others, new Message(SERVER_MESSAGE, joinMessage));
    }

    public void informOthersLeave(Session session, List<Session> others) {
        final String joinMessage = UserUtils.getUserName(session) + " left the chat!";
        sendToOthers(others, new Message(SERVER_MESSAGE, joinMessage));
    }

    public void sendToOthers(List<Session> others, Message message) {
        others.stream().forEach(
            otherSession -> sendMessage(otherSession, message));
    }

    public void sendMessage(Session session, Message message) {
        try {
            objectMapper.writeValue(session.getBasicRemote().getSendWriter(), message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
