# tyrus-sample

Quick example how-to use websockets in Java. Basically the app is a one room chat. 

It uses two ports on the localhost:
- 8025 for the websockets and 
- 8090 to serve the HTML page which contains the websocket client.


Build a single executable jar use the command:
~~~~
   mvn clean compile assembly:single
~~~
